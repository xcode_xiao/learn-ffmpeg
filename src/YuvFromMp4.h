//
// Created by xiaolei on 24-2-4.
//

#ifndef YUVFROMMP4_H
#define YUVFROMMP4_H


class YuvFromMp4
{
public:
    static int show();
    static void yuv420(const char* path, int width, int height);
};


#endif //YUVFROMMP4_H
