//
// Created by xiaolei on 24-2-1.
//

#include "SimplePlayer.h"
#include <iostream>
#include "SDL2/SDL.h"

extern "C" {
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libswscale/swscale.h"
#include "libavutil/imgutils.h"
}



int SimplePlayer::show()
{
    // FFmpeg
    AVFormatContext* pFormatCtx;
    int i, videoindex;
    AVCodecContext* pCodecCtx;
    AVCodec* pCodec;
    AVFrame *pFrame, *pFrameYUV;
    unsigned char* out_buffer;
    AVPacket* packet;
    int y_size;
    int ret, got_picture;
    struct SwsContext* img_convert_ctx;
    char filepath[] = "/home/xiaolei/CLionProjects/FFmpegPlayer/assets/video_receive_raw.h265";
    // SDL
    int screen_w, screen_h;
    SDL_Window* screen;
    SDL_Renderer* sdlRenderer;
    SDL_Texture* sdlTexture;
    SDL_Rect sdlRect;

    av_register_all();
    avformat_network_init();
    pFormatCtx = avformat_alloc_context();

    if (avformat_open_input(&pFormatCtx, filepath, nullptr, nullptr) != 0)
    {
        std::cout << "Couldn't open input stream." << std::endl;
        return -1;
    }
    if (avformat_find_stream_info(pFormatCtx, nullptr) < 0)
    {
        std::cout << "Couldn't find stream information." << std::endl;
        return -1;
    }
    videoindex = -1;
    for (i = 0; pFormatCtx->nb_streams; i++)
    {
        if (pFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO)
        {
            videoindex = i;
            break;
        }
    }
    if (videoindex == -1)
    {
        std::cout << "Didn't find a video stream." << std::endl;
        return -1;
    }

    pCodecCtx = pFormatCtx->streams[videoindex]->codec;
    pCodec = avcodec_find_decoder(pCodecCtx->codec_id);
    if (pCodec == nullptr)
    {
        std::cout << "Codec not found.";
        return -1;
    }
    if (avcodec_open2(pCodecCtx, pCodec, nullptr) < 0)
    {
        std::cout << "Could not open codec.";
        return -1;
    }

    pFrame = av_frame_alloc();
    pFrameYUV = av_frame_alloc();
    out_buffer = (unsigned char*)av_malloc(
        av_image_get_buffer_size(
            AV_PIX_FMT_YUV420P,
            pCodecCtx->width,
            pCodecCtx->height,
            1
        )
    );

    av_image_fill_arrays(
        pFrameYUV->data,
        pFrameYUV->linesize,
        out_buffer,
        AV_PIX_FMT_YUV420P,
        pCodecCtx->width,
        pCodecCtx->height,
        1
    );

    packet = (AVPacket*)av_malloc(sizeof(AVPacket));

    std::cout << "------File Information------" << std::endl;
    av_dump_format(pFormatCtx, 0, filepath, 0);
    std::cout << "------File Information------" << std::endl;


    img_convert_ctx = sws_getContext(
        pCodecCtx->width, pCodecCtx->height,
        pCodecCtx->pix_fmt,
        pCodecCtx->width, pCodecCtx->height,
        AV_PIX_FMT_YUV420P,
        SWS_BICUBIC,
        nullptr,
        nullptr,
        nullptr
    );
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_TIMER))
    {
        printf("Could not initialize SDL - %s\n", SDL_GetError());
        return -1;
    }
    screen_w = pCodecCtx->width;
    screen_h = pCodecCtx->height;

    //SDL 2.0 Support for multiple windows
    screen = SDL_CreateWindow("Simplest ffmpeg player's Window", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                              screen_w, screen_h,
                              SDL_WINDOW_OPENGL);
    if (!screen)
    {
        printf("SDL: could not create window - exiting:%s\n", SDL_GetError());
        return -1;
    }
    sdlRenderer = SDL_CreateRenderer(screen, -1, 0);
    //IYUV: Y + U + V  (3 planes)
    //YV12: Y + V + U  (3 planes)
    sdlTexture = SDL_CreateTexture(
        sdlRenderer,
        SDL_PIXELFORMAT_IYUV,
        SDL_TEXTUREACCESS_STREAMING,
        pCodecCtx->width,
        pCodecCtx->height
    );

    sdlRect.x = 0;
    sdlRect.y = 0;
    sdlRect.w = screen_w;
    sdlRect.h = screen_h;
    //SDL End----------------------

    while (av_read_frame(pFormatCtx, packet) >= 0)
    {
        if (packet->stream_index == videoindex)
        {
            ret = avcodec_decode_video2(pCodecCtx, pFrame, &got_picture, packet);
            if (ret < 0)
            {
                printf("Decode Error.\n");
                return -1;
            }
            if (got_picture)
            {
                sws_scale(
                    img_convert_ctx,
                    (const unsigned char* const*)pFrame->data,
                    pFrame->linesize,
                    0,
                    pCodecCtx->height,
                    pFrameYUV->data,
                    pFrameYUV->linesize
                );

                //SDL---------------------------
                SDL_UpdateYUVTexture(
                    sdlTexture,
                    &sdlRect,
                    pFrameYUV->data[0],
                    pFrameYUV->linesize[0],
                    pFrameYUV->data[1],
                    pFrameYUV->linesize[1],
                    pFrameYUV->data[2],
                    pFrameYUV->linesize[2]
                );

                SDL_RenderClear(sdlRenderer);
                SDL_RenderCopy(sdlRenderer, sdlTexture, nullptr, &sdlRect);
                SDL_RenderPresent(sdlRenderer);
                //SDL End-----------------------
                //Delay 40ms
                SDL_Delay(40);
            }
        }
        av_free_packet(packet);
    }
    //flush decoder
    //FIX: Flush Frames remained in Codec
    sws_freeContext(img_convert_ctx);

    SDL_Quit();

    av_frame_free(&pFrameYUV);
    av_frame_free(&pFrame);
    avcodec_close(pCodecCtx);
    avformat_close_input(&pFormatCtx);

    return 0;
}

