//
// Created by xiaolei on 24-2-5.
//

#include "SDLSimple.h"
#include <iostream>
#include <thread>
#include <memory>


using namespace std;

int SDLSimple::show()
{
    auto windowWidth = 854,
         windowHeight = 480,
         videoWidth = 854,
         videoHeight = 854;

    if (SDL_Init(SDL_INIT_VIDEO))
    {
        cout << "SDL Init Failed" << endl;
        return -1;
    }
    cout << "SDL Init Success" << endl;
    const auto window = SDL_CreateWindow(
        "",
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        windowWidth,
        windowHeight,
        SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE
    );
    if (!window)
    {
        cout << "CreateWindow Fail" << endl;
        return -1;
    }
    cout << "CreateWindow Success" << endl;
    const auto render = SDL_CreateRenderer(window, -1, 0);
    if (!render)
    {
        cout << "CreateRender Fail" << endl;
        return -1;
    }
    cout << "CreateRender Success" << endl;

    constexpr auto pixFormat = SDL_PIXELFORMAT_IYUV;

    const auto texture = SDL_CreateTexture(
        render,
        pixFormat,
        SDL_TEXTUREACCESS_STREAMING,
        videoWidth,
        videoHeight
    );
    if (!texture)
    {
        cout << "CreateTexture Fail";
        return -1;
    }
    cout << "CreateTexture Success";

    auto videoRect = new SDL_Rect();

    thread eventThread(waitEvents, render, texture, videoRect);
    eventThread.join();
    return 0;
}

void SDLSimple::waitEvents(
    SDL_Renderer* render,
    SDL_Texture* texture,
    const SDL_Rect* videoRect
)
{
    auto close = make_shared<bool>(false);
    thread waitSystemEventThread(waitSystemEvent, render, texture, videoRect, close);
    thread waitRenderEventThread(waitRenderEvent, render, texture, videoRect, close);
    waitSystemEventThread.join();
    waitRenderEventThread.join();
}

void SDLSimple::waitSystemEvent(
    SDL_Renderer* render,
    SDL_Texture* texture,
    const SDL_Rect* videoRect,
    const shared_ptr<bool>& close
)
{
    SDL_Event event;
    while (SDL_WaitEvent(&event))
    {
        if (event.type == SDL_QUIT)
        {
            *close = true;
            return;
        }
    }
    SDL_Quit();
}

void SDLSimple::waitRenderEvent(
    SDL_Renderer* render,
    SDL_Texture* texture,
    const SDL_Rect* videoRect,
    const shared_ptr<bool>& close
)
{
    while (!*close)
    {
        SDL_RenderClear(render);
        SDL_RenderCopy(render, texture, nullptr, videoRect);
        SDL_RenderPresent(render);
        SDL_Delay(40);
    }
}
