//
// Created by xiaolei on 24-2-5.
//

#ifndef SDLSIMPLE_H
#define SDLSIMPLE_H
#include <memory>

extern "C" {
#include "SDL2/SDL.h"
}

using namespace std;

class SDLSimple
{
public:
    static int show();
    static void waitEvents(
        SDL_Renderer* render,
        SDL_Texture* texture,
        const SDL_Rect* videoRect
    );
    static void waitSystemEvent(
        SDL_Renderer* render,
        SDL_Texture* texture,
        const SDL_Rect* videoRect,
        const shared_ptr<bool>& close
    );
    static void waitRenderEvent(
        SDL_Renderer* render,
        SDL_Texture* texture,
        const SDL_Rect* videoRect,
        const shared_ptr<bool>& close
    );
};


#endif //SDLSIMPLE_H
