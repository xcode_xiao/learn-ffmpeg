//
// Created by xiaolei on 24-2-2.
//

#include "SimpleWindow.h"
#include <iostream>
#include "SDL2/SDL.h"

extern "C" {
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libswscale/swscale.h"
#include "libavutil/imgutils.h"
}

int SimpleWindow::show()
{
    if (SDL_Init(SDL_INIT_EVENTS) == -1)
    {
        std::cout << "sdl init fail";
        return -1;
    }

    SDL_Window* window = SDL_CreateWindow(
        "SDL2 Draw Window",
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        640,
        480,
        SDL_WINDOW_SHOWN
    );
    if (window == nullptr)
    {
        std::cout << "Could not create window:%s\n" << SDL_GetError() << std::endl;
        return -1;
    }
    SDL_Renderer* render = SDL_CreateSoftwareRenderer(SDL_GetWindowSurface(window));
    SDL_SetRenderDrawColor(render, 255, 255, 255, 255);
    SDL_RenderClear(render);
    SDL_SetRenderDrawColor(render, 0, 0, 0, 255);
    SDL_RenderDrawPoint(render, 10, 10);

    SDL_RenderPresent(render);
    SDL_UpdateWindowSurface(window);

    bool quit = false;
    SDL_Event event;
    while (!quit)
    {
        SDL_WaitEvent(&event);

        switch (event.type)
        {
        case SDL_QUIT:
            SDL_Log("quit");
            quit = true;
            break;
        default:
            SDL_Log("event type:%d", event.type);
        }
    }

    if (render)
        SDL_DestroyRenderer(render);
    SDL_DestroyWindow(window);

    SDL_Quit();
    return 0;
}
