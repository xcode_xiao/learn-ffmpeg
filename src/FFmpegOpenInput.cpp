//
// Created by xiaolei on 24-2-4.
//

#include "FFmpegOpenInput.h"

extern "C" {
#include <libavformat/avformat.h>
#include <libavutil/opt.h>
}

int FFmpegOpenInput::show()
{
    av_register_all();
    avformat_network_init();
    AVFormatContext* pFormat = nullptr;
    // const auto path = "/home/xiaolei/CLionProjects/FFmpegPlayer/assets/test.mp4";
    const auto path = "http://devimages.apple.com.edgekey.net/streaming/examples/bipbop_4x3/gear2/prog_index.m3u8";

    AVDictionary* opt = nullptr;
    av_dict_set(&opt, "rtsp_transport", "tcp", 0);
    av_dict_set(&opt, "max_delay", "550", 0);

    if (const int result = avformat_open_input(&pFormat, path, nullptr, &opt); result)
    {
        std::cout << "avformat_open_input fail" << std::endl;
        return -1;
    }
    std::cout << "avformat_open_input success" << std::endl;
    if (avformat_find_stream_info(pFormat, nullptr))
    {
        std::cout << "avformat_find_stream_info fail" << std::endl;
    }
    std::cout << "avformat_find_stream_info success" << std::endl;
    const int64_t time = pFormat->duration;
    const int64_t min = (time / 1000000) / 60;
    const int64_t second = (time / 1000000) % 60;
    std::cout << min << ":" << second << std::endl;

    av_dump_format(pFormat, 0, path, 0);

    return 0;
}
