//
// Created by xiaolei on 24-2-2.
//

#include "SimpleEventPlayer.h"
#include <iostream>
#include "SDL2/SDL.h"

extern "C" {
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libswscale/swscale.h"
#include "libavutil/imgutils.h"
}

#define SFM_REFRESH_EVENT (SDL_USEREVENT + 1)
#define SFM_BREAK_EVENT (SDL_USEREVENT + 2)

int thread_exit = 0;
int thread_pause = 0;

int sfp_refresh_thread(void* opaque)
{
    thread_exit = 0;
    thread_pause = 0;
    while (!thread_exit)
    {
        if (!thread_pause)
        {
            SDL_Event event;
            event.type = SFM_REFRESH_EVENT;
            SDL_PushEvent(&event);
        }
        SDL_Delay(5);
    }
    thread_exit = 0;
    thread_pause = 0;

    SDL_Event event;
    event.type = SFM_BREAK_EVENT;
    SDL_PushEvent(&event);

    return 0;
}

int SimpleEventPlayer::show()
{
    AVFormatContext* pFormatCtx;
    AVFrame *pFrame, *pFrameYUV;
    int got_picture;

    //------------SDL----------------
    SDL_Rect sdlRect;
    SDL_Event event;

    //char filepath[]="bigbuckbunny_480x272.h265";
    char filepath[] = "/home/xiaolei/CLionProjects/FFmpegPlayer/assets/video_receive_raw.h265";

    av_register_all();
    avformat_network_init();
    pFormatCtx = avformat_alloc_context();

    if (avformat_open_input(&pFormatCtx, filepath, nullptr, nullptr) != 0)
    {
        printf("Couldn't open input stream.\n");
        return -1;
    }
    if (avformat_find_stream_info(pFormatCtx, nullptr) < 0)
    {
        printf("Couldn't find stream information.\n");
        return -1;
    }
    int videoindex = -1;
    for (int i = 0; i < pFormatCtx->nb_streams; i++)
        if (pFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO)
        {
            videoindex = i;
            break;
        }
    if (videoindex == -1)
    {
        printf("Didn't find a video stream.\n");
        return -1;
    }
    AVCodecContext* pCodecCtx = pFormatCtx->streams[videoindex]->codec;
    const AVCodec* pCodec = avcodec_find_decoder(pCodecCtx->codec_id);
    if (pCodec == nullptr)
    {
        printf("Codec not found.\n");
        return -1;
    }
    if (avcodec_open2(pCodecCtx, pCodec, nullptr) < 0)
    {
        printf("Could not open codec.\n");
        return -1;
    }
    pFrame = av_frame_alloc();
    pFrameYUV = av_frame_alloc();

    unsigned char* out_buffer = static_cast<unsigned char*>(av_malloc(
        av_image_get_buffer_size(
            AV_PIX_FMT_YUV420P,
            pCodecCtx->width,
            pCodecCtx->height,
            1
        )
    ));
    av_image_fill_arrays(
        pFrameYUV->data,
        pFrameYUV->linesize,
        out_buffer,
        AV_PIX_FMT_YUV420P,
        pCodecCtx->width,
        pCodecCtx->height,
        1
    );

    //Output Info-----------------------------
    printf("---------------- File Information ---------------\n");
    av_dump_format(pFormatCtx, 0, filepath, 0);
    printf("-------------------------------------------------\n");

    struct SwsContext* img_convert_ctx = sws_getContext(
        pCodecCtx->width,
        pCodecCtx->height,
        pCodecCtx->pix_fmt,
        pCodecCtx->width,
        pCodecCtx->height,
        AV_PIX_FMT_YUV420P,
        SWS_BICUBIC,
        NULL,
        NULL,
        NULL
    );


    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_TIMER))
    {
        printf("Could not initialize SDL - %s\n", SDL_GetError());
        return -1;
    }
    //SDL 2.0 Support for multiple windows
    int screen_w = pCodecCtx->width;
    int screen_h = pCodecCtx->height;
    SDL_Window* screen = SDL_CreateWindow(
        "Simplest ffmpeg player's Window",
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        screen_w,
        screen_h,
        SDL_WINDOW_OPENGL
    );

    if (!screen)
    {
        printf("SDL: could not create window - exiting:%s\n", SDL_GetError());
        return -1;
    }
    SDL_Renderer* sdlRenderer = SDL_CreateRenderer(screen, -1, 0);
    //IYUV: Y + U + V  (3 planes)
    //YV12: Y + V + U  (3 planes)
    SDL_Texture* sdlTexture = SDL_CreateTexture(
        sdlRenderer,
        SDL_PIXELFORMAT_IYUV,
        SDL_TEXTUREACCESS_STREAMING,
        pCodecCtx->width,
        pCodecCtx->height
    );

    sdlRect.x = 0;
    sdlRect.y = 0;
    sdlRect.w = screen_w;
    sdlRect.h = screen_h;

    auto packet = static_cast<AVPacket*>(av_malloc(sizeof(AVPacket)));

    SDL_CreateThread(sfp_refresh_thread, "video_render_thread",NULL);
    //------------SDL End------------
    //Event Loop

    for (;;)
    {
        //Wait
        SDL_WaitEvent(&event);
        time_t time_now = 0;
        time(&time_now);
        const auto str = new char[300];
        sprintf(str, "event:%x", event.type);
        std::cout << str << " -> time:" << asctime(gmtime(&time_now)) << std::endl;
        delete str;
        if (event.type == SFM_REFRESH_EVENT)
        {
            while (true)
            {
                if (av_read_frame(pFormatCtx, packet) < 0)
                    thread_exit = 1;

                if (packet->stream_index == videoindex)
                    break;
            }
            int ret = avcodec_decode_video2(
                pCodecCtx,
                pFrame,
                &got_picture,
                packet
            );
            if (ret < 0)
            {
                printf("Decode Error.\n");
                return -1;
            }
            if (got_picture)
            {
                sws_scale(
                    img_convert_ctx,
                    (const unsigned char* const*)pFrame->data,
                    pFrame->linesize,
                    0,
                    pCodecCtx->height,
                    pFrameYUV->data,
                    pFrameYUV->linesize
                );
                //SDL---------------------------
                SDL_UpdateTexture(
                    sdlTexture,
                    NULL,
                    pFrameYUV->data[0],
                    pFrameYUV->linesize[0]
                );
                SDL_RenderClear(sdlRenderer);
                //SDL_RenderCopy( sdlRenderer, sdlTexture, &sdlRect, &sdlRect );  
                SDL_RenderCopy(
                    sdlRenderer,
                    sdlTexture,
                    NULL,
                    NULL
                );
                SDL_RenderPresent(sdlRenderer);
                //SDL End-----------------------
            }
            av_free_packet(packet);
        }
        else if (event.type == SDL_KEYDOWN)
        {
            //Pause
            if (event.key.keysym.sym == SDLK_SPACE)
                thread_pause = !thread_pause;
        }
        else if (event.type == SDL_QUIT)
        {
            thread_exit = 1;
        }
        else if (event.type == SFM_BREAK_EVENT)
        {
            break;
        }
    }

    sws_freeContext(img_convert_ctx);

    SDL_Quit();
    //--------------
    av_frame_free(&pFrameYUV);
    av_frame_free(&pFrame);
    avcodec_close(pCodecCtx);
    avformat_close_input(&pFormatCtx);

    return 0;
}
