//
// Created by xiaolei on 24-2-4.
//

#include "AVDecodeSimple.h"
#include <iostream>

extern "C" {
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libavutil/opt.h>
}

int AVDecodeSimple::show()
{
    av_register_all();
    avformat_network_init();
    AVFormatContext* pFormat = nullptr;
    const auto path = "/home/xiaolei/CLionProjects/FFmpegPlayer/assets/test.mp4";

    AVDictionary* opt = nullptr;
    av_dict_set(&opt, "rtsp_transport", "tcp", 0);
    av_dict_set(&opt, "max_delay", "550", 0);

    if (const int result = avformat_open_input(&pFormat, path, nullptr, &opt); result)
    {
        std::cout << "avformat_open_input fail" << std::endl;
        return -1;
    }
    std::cout << "avformat_open_input success" << std::endl;
    if (avformat_find_stream_info(pFormat, nullptr))
    {
        std::cout << "avformat_find_stream_info fail" << std::endl;
    }
    std::cout << "avformat_find_stream_info success" << std::endl;
    const int64_t time = pFormat->duration;
    const int64_t min = (time / 1000000) / 60;
    const int64_t second = (time / 1000000) % 60;
    std::cout << min << ":" << second << std::endl;

    av_dump_format(pFormat, 0, path, 0);

    int videoStreamIndex = -1, audioStreamIndex = -1;
    videoStreamIndex = av_find_best_stream(pFormat, AVMEDIA_TYPE_VIDEO, -1, -1, nullptr,NULL);
    audioStreamIndex = av_find_best_stream(pFormat, AVMEDIA_TYPE_AUDIO, -1, -1, nullptr,NULL);

    const AVCodec* vCodec = avcodec_find_decoder(pFormat->streams[videoStreamIndex]->codec->codec_id);
    if (!vCodec)
    {
        std::cout << "avcodec_find_decoder fail" << std::endl;
        return -1;
    }
    std::cout << "avcodec_find_decoder success" << std::endl;

    if (avcodec_open2(pFormat->streams[videoStreamIndex]->codec, vCodec, nullptr))
    {
        std::cout << "avcodec_open2 fail" << std::endl;
        return -1;
    }
    std::cout << "avcodec_open2 success" << std::endl;

    // alloc memory space
    AVFrame* frame = av_frame_alloc();
    AVFrame* frameYuv = av_frame_alloc();

    const int width = pFormat->streams[videoStreamIndex]->codec->width;
    const int height = pFormat->streams[videoStreamIndex]->codec->height;
    const AVPixelFormat pixFmt = pFormat->streams[videoStreamIndex]->codec->pix_fmt;

    const int size = avpicture_get_size(AV_PIX_FMT_YUV420P, width, height);

    const uint8_t* buff = nullptr;
    buff = static_cast<uint8_t*>(av_malloc(size));

    avpicture_fill(reinterpret_cast<AVPicture*>(frameYuv), buff, AV_PIX_FMT_YUV420P, width, height);


    const auto packate = static_cast<AVPacket*>(av_malloc(sizeof(AVPacket)));
    SwsContext* swsCtx = sws_getContext(
        width,
        height,
        pixFmt,
        width,
        height,
        AV_PIX_FMT_YUV420P,
        SWS_BICUBIC,
        nullptr,
        nullptr,
        nullptr
    );

    // read frame
    int got_picture = 0;
    int frameCount = 0;
    FILE* yuv_y = fopen("yuv_y.y", "wb+");
    FILE* yuv_u = fopen("yuv_u.u", "wb+");
    FILE* yuv_v = fopen("yuv_v.v", "wb+");
    while (av_read_frame(pFormat, packate) >= 0)
    {
        if (packate->stream_index == videoStreamIndex)
        {
            if (const int result = avcodec_decode_video2(
                pFormat->streams[videoStreamIndex]->codec,
                frame,
                &got_picture,
                packate
            ); result < 0)
            {
                std::cout << "avcodec_decode_video2 fail" << std::endl;
                return -1;
            }
            if (got_picture)
            {
                sws_scale(
                    swsCtx,
                    frame->data,
                    frame->linesize,
                    0,
                    height,
                    frameYuv->data,
                    frameYuv->linesize
                );

                fwrite(frameYuv->data[0], 1, width * height, yuv_y);
                fwrite(frameYuv->data[1], 1, width * height / 4, yuv_y);
                fwrite(frameYuv->data[2], 1, width * height / 4, yuv_y);

                frameCount++;
                std::cout << "frameCount:" << frameCount << std::endl;
            }
        }
        av_free_packet(packate);
    }
    fclose(yuv_y);
    fclose(yuv_u);
    fclose(yuv_v);
    
    sws_freeContext(swsCtx);
    av_frame_free(&frame);
    av_frame_free(&frameYuv);
    avformat_close_input(&pFormat);

    return 0;
}
